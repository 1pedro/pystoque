#-*- encoding:utf-8 -*-
# importando o flask
from flask import Flask, render_template
from wtforms import Form, TextField, TextAreaField,\
                    validators, StringField, SubmitField
import pyodbc


app = Flask('sequencial', template_folder='app/templates', static_folder="app/static")

# Configurações
app.config.from_object('config')


db = pyodbc.connect("Driver={DRIVER};SERVER={SERVER};PORT={PORT};DATABASE={DATABASE};"
                    "UID={UID};PWD={PWD};TDS_Version={TDS_VERSION}".format(
                        DRIVER=app.config['DRIVER'],
                        SERVER=app.config['SERVER'],
                        PORT=app.config['PORT'],
                        DATABASE=app.config['DATABASE'],
                        UID=app.config['UID'],
                        PWD=app.config['PWD'],
                        TDS_VERSION=app.config['TDS_VERSION'])
                    )

# Importando modulo de sequencial_view
from .sequencial.controllers import sequencial as seq_module


# Rota e pagina de Erro 404
@app.errorhandler(404)
def not_found(error):
    return render_template("404.html"), 404

# Registrando o modulo de sequencial_view
app.register_blueprint(seq_module)

