from flask import Blueprint, request, render_template, \
                  flash, g, session, redirect, url_for
from app import db
from app.sequencial.forms import *

sequencial = Blueprint('seq', __name__, url_prefix='/')
cursor = db.cursor()


@sequencial.route('/', methods=['GET', 'POST'])
def f_sequencial():
    form = SequencialForm(request.form)

    if request.method == 'POST':
        sequenciais = str(request.form['sequenciais']).split()
        f_string = "("
        for item in list(sequenciais):
            try:
                a = int(item)
                f_string = f_string + "'{item}',".format(item=item)
            except ValueError as e:
                print(e, a)

        #lista = []

        lista = get_lotes_x_sequenciais(f_string[:-1]+")")


    if request.form.get('sequenciais', None):

        if form.validate():
            if lista is None:
                flash("Sequencial Inválido.", category="error")
            else:
                for item in lista:
                    flash(item, category="success")
        else:
            flash('All the form fields are required.')


    return render_template("sequencial_view/hello.html", form=form)


def get_lotes_x_sequenciais(sequenciais):
    query = """
                SELECT D5_PRODUTO, D5_LOCAL, D5_LOTECTL, D5_QUANT, D5_FSSEQ
                FROM SD5010 WHERE D5_FSSEQ IN {sequenciais}
            """.format(sequenciais=sequenciais)

    cursor.execute(query)
    data = cursor.fetchall()
    if data:
        return data