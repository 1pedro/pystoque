#!/usr/bin/env python3

import pyodbc

cnxn = pyodbc.connect('DRIVER={FreeTDS};SERVER=server;PORT=1433;DATABASE=table;UID=user;PWD=password;TDS_Version=7.0')

cursor = cnxn.cursor()


cursor.execute("select D5_FSSEQ, D5_LOTECTL from SD5010 where D5_FSSEQ = 0303959")
row = cursor.fetchone()
if row:
    print(row)
